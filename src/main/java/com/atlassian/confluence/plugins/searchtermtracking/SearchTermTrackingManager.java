package com.atlassian.confluence.plugins.searchtermtracking;

import com.atlassian.confluence.event.events.search.SearchPerformedEvent;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public interface SearchTermTrackingManager extends InitializingBean, DisposableBean
{
    void handle(SearchPerformedEvent e);
}
