package com.atlassian.confluence.plugins.searchtermtracking.events;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("grow-000000.confluence.cac.search.term.raw.query")
public class SearchTermPerformedEvent
{
    private String rawQuery;

    public SearchTermPerformedEvent(String rawQuery)
    {
        this.rawQuery = rawQuery;
    }

    public String getRawQuery()
    {
        return rawQuery;
    }
}